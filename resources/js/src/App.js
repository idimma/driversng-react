import React, { Component } from 'react';
import Home from './containers/Home'
import './styles/App.scss';

class App extends Component {
  render() {
    return (
      <>
        <Home />
      </>
    );
  }
}

export default App;
