import React from "react";
import { Tab, Tabs, TabList } from "react-tabs";

import Header from "../../components/Header";
import Footer from "../../components/Footer";
import DownloadSection from "../../components/DownloadSection";
import HireVehiclesSection from "../../components/HireVehiclesSection";
import HireVehicleTabContent from "./_partials/HireVehiclesTabContent";
import HotelsContent from "./_partials/HotelsContent";
import FlightsContent from "./_partials/FlightsContent";
import RecruitContent from "./_partials/RecruitContent";
import FleetContent from "./_partials/FleetContent";

// React Tabs CSS File, it's required for the tabs to work properly;
import "react-tabs/style/react-tabs.css";

class Home extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      sectionToShow: <HireVehiclesSection />
    };

    this.onSwitchSection = this.onSwitchSection.bind(this)
  }

  onSwitchSection (tabIndex) {
    if (tabIndex === 0)
      this.setState({ sectionToShow: <HireVehiclesSection /> });
    if (tabIndex === 1) this.setState({ sectionToShow: <HotelsContent /> });
    if (tabIndex === 2) this.setState({ sectionToShow: <FlightsContent /> });
    if (tabIndex === 3) this.setState({ sectionToShow: <RecruitContent /> });
    if (tabIndex === 4) this.setState({ sectionToShow: "Subcriptions" });
    if (tabIndex === 5) this.setState({ sectionToShow: <FleetContent /> });
  };

  render() {
    const { sectionToShow } = this.state;
    return (
      <div>
        <main id="home">
          <div className="hero">
            <Header />

            <div className="home-tabs__container mt-5 container">
              <Tabs onSelect={tabIndex => this.onSwitchSection(tabIndex)}>
                <TabList>
                  <Tab>Hire Vehicles</Tab>
                  <Tab>Hotels</Tab>
                  <Tab>Flights</Tab>
                  <Tab>Recruit by Myself</Tab>
                  <Tab>Subcriptions</Tab>
                  <Tab>Fleet Mgt.</Tab>
                </TabList>
                <div className="tab-panel">
                  <HireVehicleTabContent />
                </div>
              </Tabs>
            </div>
          </div>
          {sectionToShow}
          <DownloadSection />
        </main>
        <Footer />
      </div>
    );
  }
}

export default Home;
