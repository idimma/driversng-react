import React from "react";

const HireVehicleTabContent = () => {
  return (
    <div>
      <div className="row">
        <div className="col-12 text-center">
          <h2 className="mb-3">Mobility Solution at Your Finger Tip</h2>
          <h4>Drivers | Vehicles | MarketPlace</h4>
        </div>
      </div>

      <div className="row mt-4 no-gutters justify-content-center">
        <div className="col-md-3 p-1">
          <input
            type="text"
            className="form-control d-block"
            placeholder="Driver Use"
            id="driveUse"
          />
        </div>
        <div className="col-md-3 p-1">
          <input
            type="text"
            className="form-control d-block"
            placeholder="Service Type"
            id="ServiceType"
          />
        </div>
        <div className="col-md-3 p-1">
          <input
            type="text"
            className="form-control d-block"
            placeholder="Location"
            id="location"
          />
        </div>
        <div className="col-md-3 p-1">
          <button className="btn btn-warning">Search</button>
        </div>
      </div>

      <h5 className="mt-3">Call/ WhatsApp 070-6248-3241 OR 081-6355-5265 (Calls Only)</h5>
    </div>
  );
};

export default HireVehicleTabContent;
