import React from "react";

class FleetContent extends React.PureComponent {
  render() {
    return (
      <div className="page-tab-content">
        <div className="container">
          <div className="mb-4">
            <div className="row">
              <div className="col-md-5">
                <button
                  className="btn btn-outline-primary btn-block"
                  type="button"
                  data-toggle="collapse"
                  data-target="#diamondService"
                  aria-expanded="false"
                  aria-controls="diamondService"
                >
                  <i className="fas fa-plus mr-2" />
                  Diamond Service {`{Fleet Management}`}
                </button>
              </div>
            </div>
            <div className="collapse" id="diamondService">
              <div className="card card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life
                accusamus terry richardson ad squid. Nihil anim keffiyeh
                helvetica, craft beer labore wes anderson cred nesciunt sapiente
                ea proident.
              </div>
            </div>
          </div>
          <div className="mb-4">
            <div className="row">
              <div className="col-md-5">
                <button
                  className="btn btn-outline-primary btn-block"
                  type="button"
                  data-toggle="collapse"
                  data-target="#trainingService"
                  aria-expanded="false"
                  aria-controls="trainingService"
                >
                  <i className="fas fa-plus mr-2" />
                  Training as a Service
                </button>
              </div>
            </div>
            <div className="collapse" id="trainingService">
              <div className="card card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life
                accusamus terry richardson ad squid. Nihil anim keffiyeh
                helvetica, craft beer labore wes anderson cred nesciunt sapiente
                ea proident.
              </div>
            </div>
          </div>
          <div className="mb-4">
            <div className="row">
              <div className="col-md-5">
                <button
                  className="btn btn-outline-primary btn-block"
                  type="button"
                  data-toggle="collapse"
                  data-target="#verificationService"
                  aria-expanded="false"
                  aria-controls="verificationService"
                >
                  <i className="fas fa-plus mr-2" />
                  Verification as a Service
                </button>
              </div>
            </div>
            <div className="collapse" id="verificationService">
              <div className="card card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life
                accusamus terry richardson ad squid. Nihil anim keffiyeh
                helvetica, craft beer labore wes anderson cred nesciunt sapiente
                ea proident.
              </div>
            </div>
          </div>
          <div className="mb-4">
            <div className="row">
              <div className="col-md-5">
                <button
                  className="btn btn-outline-primary btn-block"
                  type="button"
                  data-toggle="collapse"
                  data-target="#fleetValuation"
                  aria-expanded="false"
                  aria-controls="fleetValuation"
                >
                  <i className="fas fa-plus mr-2" />
                  Fleet/Asset Valuation
                </button>
              </div>
            </div>
            <div className="collapse" id="fleetValuation">
              <div className="card card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life
                accusamus terry richardson ad squid. Nihil anim keffiyeh
                helvetica, craft beer labore wes anderson cred nesciunt sapiente
                ea proident.
              </div>
            </div>
          </div>
          <div className="mb-4">
            <div className="row">
              <div className="col-md-5">
                <button
                  className="btn btn-outline-primary btn-block"
                  type="button"
                  data-toggle="collapse"
                  data-target="#comprehensiveInsurance"
                  aria-expanded="false"
                  aria-controls="comprehensiveInsurance"
                >
                  <i className="fas fa-plus mr-2" />
                  Comprehensive Insurance
                </button>
              </div>
            </div>
            <div className="collapse" id="comprehensiveInsurance">
              <div className="card card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life
                accusamus terry richardson ad squid. Nihil anim keffiyeh
                helvetica, craft beer labore wes anderson cred nesciunt sapiente
                ea proident.
              </div>
            </div>
          </div>
          <div className="mb-4">
            <div className="row">
              <div className="col-md-5">
                <button
                  className="btn btn-outline-primary btn-block"
                  type="button"
                  data-toggle="collapse"
                  data-target="#fleetManagement"
                  aria-expanded="false"
                  aria-controls="fleetManagement"
                >
                  <i className="fas fa-plus mr-2" />
                  Fleet/Asset Management
                </button>
              </div>
            </div>
            <div className="collapse" id="fleetManagement">
              <div className="card card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life
                accusamus terry richardson ad squid. Nihil anim keffiyeh
                helvetica, craft beer labore wes anderson cred nesciunt sapiente
                ea proident.
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FleetContent;
