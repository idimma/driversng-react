import React from "react";

class HotelsContent extends React.PureComponent {
  render() {
    return (
      <div className="page-tab-content">
        <div className="container">
          <div className="row justify-content-between">
            <div className="col-md-6">
              <div className="tab-form">
                <div className="row">
                  <div className="col-2">
                    <span className="step-number__label">1</span>
                  </div>
                  <div className="col-10 mb-4">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Name of Hotel"
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <span className="step-number__label">2</span>
                  </div>
                  <div className="col-10 mb-4">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Hotel Location/Address"
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <span className="step-number__label">3</span>
                  </div>
                  <div className="col-10">
                    <div className="row">
                      <div className="col-md-6 mb-4">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Pick-up Location"
                        />
                      </div>
                      <div className="col-md-6 mb-4">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Drop-off Location"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <span className="step-number__label">4</span>
                  </div>
                  <div className="col-10 mb-4">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Pick-up Date"
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <span className="step-number__label">5</span>
                  </div>
                  <div className="col-10 mb-4">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Pick-up Time"
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <span className="step-number__label">6</span>
                  </div>
                  <div className="col-10">
                    <div className="row">
                      <div className="col-md-6 mb-4">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="One-off"
                        />
                      </div>
                      <div className="col-md-6 mb-4">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Return"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">
                    <span className="step-number__label">7</span>
                  </div>
                  <div className="col-10">
                    <div className="row">
                      <div className="col-md-6 mb-4">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Driver Only"
                        />
                      </div>
                      <div className="col-md-6 mb-4">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Vehicle + Driver"
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row justify-content-center">
                  <div className="col-5 text-center mt-4">
                    <button className="btn btn-warning">Get Quote</button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-5 mt-4">
              <h4 className="mb-4">Are you trying to book your Hotels?</h4>

              <p className="mb-3">
                You can try any of these websites Hotels.ng, JumiaTravels,
                Wakanow.
              </p>

              <p>
                After you have booked your favourite flights, use our Airport
                transfer service to your airport destination or are you coming
                into the country, we can organize a privatr transport move for
                your comfort and style.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HotelsContent;
