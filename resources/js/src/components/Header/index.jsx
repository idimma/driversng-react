import React, { useState } from 'react'

const Header = (props) => {

  const [showMenu, setShowMenu] = useState(false)

  
  return (
    <header>
      <div className="app-logo__container">
        <img src="/imgs/app-logo.png" alt="App Logo"/>
      </div>
      <button className="menu-toggle__btn" onClick={() => {setShowMenu(!showMenu)}}>
        { showMenu ? "CLOSE" : "MENU" }
      </button>
      <nav className={showMenu ? "open" : ""}>
        <ul>
          <li><a href="">Blog</a></li>
          <li><a href="">Hubs</a></li>
          <li><a href="">Drive With Us</a></li>
          <li><a href="" className="btn btn-primary btn-outline-white">Login</a></li>
          <li><a href="" className="btn btn-primary">Get Started</a></li>
        </ul>
      </nav>
    </header>
  )
}
export default Header;