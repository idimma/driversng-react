import React from "react";
import Slider from "react-slick";

class ViewMoreSlider extends React.PureComponent {
  render() {
    const settings = {
      arrows: true,
      infinite: true,
      speed: 300,
      slidesToShow: 6,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      responsive: [
        {
          breakpoint: 320,
          settings: { slidesToShow: 1, slidesToScroll: 1, infinite: true }
        },
        {
          breakpoint: 768,
          settings: { slidesToShow: 2, slidesToScroll: 1, infinite: true }
        },
        {
          breakpoint: 1024,
          settings: { slidesToShow: 5, slidesToScroll: 1, infinite: true }
        }
      ]
    };

    return (
      <div className="view-more__slider">
        <Slider {...settings}>
          <div className="slide">
            <div className="text-center">
              <h6 className="mb-4 mt-5">FULL TERM DRIVERS</h6>
              <a href>view more</a>
            </div>
          </div>
          <div className="slide">
          <div className="text-center">
              <h6 className="mb-4 mt-5">SHORT TERM DRIVERS</h6>
              <a href>view more</a>
            </div>
          </div>
          <div className="slide">
          <div className="text-center">
              <h6 className="mb-4 mt-5">UBER <br />DRIVERS</h6>
              <a href>view more</a>
            </div>
          </div>
          <div className="slide">
          <div className="text-center">
              <h6 className="mb-4 mt-5">TAXIFY <br />DRIVERS</h6>
              <a href>view more</a>
            </div>
          </div>
          <div className="slide">
          <div className="text-center">
              <h6 className="mb-4 mt-5">BRT <br />DRIVERS</h6>
              <a href>view more</a>
            </div>
          </div>
          <div className="slide">
          <div className="text-center">
              <h6 className="mb-4 mt-5">AIRPORT <br />DRIVERS</h6>
              <a href>view more</a>
            </div>
          </div>
          <div className="slide">
          <div className="text-center">
              <h6 className="mb-4 mt-5">TUTOR <br />DRIVERS</h6>
              <a href>view more</a>
            </div>
          </div>
          <div className="slide">
          <div className="text-center">
              <h6 className="mb-4 mt-5">TUTOR <br />DRIVERS</h6>
              <a href>view more</a>
            </div>
          </div>
        </Slider>
      </div>
    );
  }
}

export default ViewMoreSlider;
