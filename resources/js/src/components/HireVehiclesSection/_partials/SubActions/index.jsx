import React from "react"

const SubActions = () => (
    <div className="sub-actions">
        <div className="row justify-content-between">
            <div className="col-md-6">
                <div className="row no-gutters">
                    <div className="col-12 col-md-4 btn-block">
                        <button className="btn-primary btn-block add-vehicle">
                            Add Vehicle
                  </button>
                    </div>
                    <div className="col-12 col-md-7">
                        <button className="btn-primary rate-drivers btn-block">
                            Rate Full Term Drivers
                  </button>
                    </div>
                </div>
            </div>
            <div className="col-md-2 text-right">
                <button className="btn-primary btn-block">CHAT NOW</button>
            </div>
        </div>
    </div>
)

export default SubActions;