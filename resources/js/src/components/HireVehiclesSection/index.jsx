import React from "react";
import ViewMoreSlider from "./_partials/ViewMoreSlider";
import SubActions from "./_partials/SubActions";

const HireVehiclesSection = () => {
  return (
    <section className="hire-vehicles">
      <div className="container">
        <div className="text-center mt-2">
          <p>
            Recruit by Yourself and Build your Own Data base of personal
            Drivers, Dispatch Riders, Bikers and many more....
          </p>
          <p>
            Discover and Checkout the best professional drivers, driving tutors
            and the likes around your location
          </p>
        </div>
        <ViewMoreSlider />
        <SubActions />
        <div className="row mt-5">
          <div className="col-md-4 text-center mb-4">
            <div className="solution-item">
              <div className="solution-item__img">
                <img src="/imgs/responsive.png" alt="Mobility Solutions" />
              </div>
              <h6 className="mt-5">Mobility Solutions</h6>
              <p className="mt-3">
                Driversng has made available multiple mobility solutions for
                corporate and personal use. You can enjoy any of the services
                from the comfort of your office or home via your laptop, phone
                or tablets
              </p>
            </div>
          </div>
          <div className="col-md-4 text-center mb-4">
            <div className="solution-item">
              <div className="solution-item__img">
                <img src="/imgs/stop-watch.png" alt="" />
              </div>
              <h6 className="mt-5">On-demand & Realtime</h6>
              <p className="mt-3">
                All our services are delivered almost immediately when you
                sign--up, request and pay for the service you want to enjoy.
                From 30 minutes to 24 hours you enjoy quick response from us
              </p>
            </div>
          </div>
          <div className="col-md-4 text-center mb-4">
            <div className="solution-item">
              <div className="solution-item__img">
                <img src="/imgs/trust.png" alt="" />
              </div>
              <h6 className="mt-5">Discipline & Security</h6>
              <p className="mt-3">
                We have taken time to pass all the necessary information to
                ensure your safty to drivers driving via our plaftorm. Drivers
                on our platform are disciplined to keep you secured & drive you
                in comfort and style
              </p>
            </div>
          </div>
        </div>
        <div className="row align-items-center mt-5">
          <div className="col-md-6 p-5">
            <img
              src="/imgs/organise-private-car.png"
              alt="Organise Private Cars"
            />
          </div>
          <div className="col-md-6">
            <div>
              <h5 className="mb-4">Organised Private Transport Program</h5>
              <p className="mb-4">
                Are you always travelling with GIGM (God is Good Motor), PMT
                (Peace Mass Transit), ABC Transport, GUO Transport, etc.
                Perhaps, you are the type that would always need to meet up with
                the very early morning more with these transport companies, So
                yes, it is you we have proferred this solution for. We can pick
                you from or drop you off at your choice travelling transport
                companies. All you need to do is to Book a day before, we would
                organise the Best move for you.
              </p>
              <a href className="btn btn-primary">
                BOOK NOW
              </a>
            </div>
          </div>
        </div>
        <div className="row align-items-center mt-5">
          <div className="col-md-6">
            <div>
              <h5 className="mb-4">Human Resourse Manager Plans</h5>
              <p className="mb-4">
                Your corporate Organization can cut down cost so as to run
                business efficiently. asa your business grows, mobility becomes
                a necessity in expanding your services/products so engaging
                already verified trained drivers for land transportation comes
                in. Hence , it is good you continually have access to drivers
                provision to make this objective always possible
              </p>
              <a href className="btn btn-primary">
                BOOK NOW
              </a>
            </div>
          </div>
          <div className="col-md-6 p-5">
            <img src="/imgs/mountain-car.png" alt="Human Resourse Manager" />
          </div>
        </div>
      </div>
      <div className="outsource-section">
        <div className="container">
          <div className="row justify-content-end">
            <div className="col-md-6 mt-5 mb-5">
              <h5 className="mb-4">Outsourcing/ Resourcing Plans</h5>
              <p className="mb-3">
                Outsource your drivers recruitment to us and focus on what
                really matters to you, or Resource drivers from us and be in
                charge. With our affordable Premium, King and Ipay plans you can
                outsourse or resource.
              </p>
              <a href className="btn btn-outline-white">
                Check Now
              </a>
            </div>
          </div>
          <div className="row justify-content-end">
            <div className="col-md-6 mt-5 mb-5">
              <h5 className="mb-4">Subscription Plans</h5>
              <p className="mb-3">
                We can drive you daily, weekly, monthly or annulally for
                whatever purpose you may desire. Subscribe and just call out for
                the service whenever you need it.
              </p>
              <a href className="btn btn-outline-white">
                Check Now
              </a>
            </div>
          </div>
        </div>
      </div>

      <div className="features">
        <div className="container">
          <h5 className="text-center mt-5 mb-5">We are also featured on</h5>
          <div className="row justify-content-center align-items-center">
            <div className="col-6 col-sm-2 mb-5">
              <img
                src="/imgs/techpoint.png"
                alt="Techpoint"
                id="techpoint-logo"
              />
            </div>
            <div className="col-6 col-sm-2 mb-5">
              <img src="/imgs/rhythm.png" alt="" id="rhythm-logo" />
            </div>
            <div className="col-6 col-sm-2 mb-5">
              <img src="/imgs/techmoran.png" alt="" id="techmoran-logo" />
            </div>
            <div className="col-6 col-sm-2 mb-5">
              <img src="/imgs/classic.png" alt="" id="classic-logo" />
            </div>
            <div className="col-6 col-sm-2 mb-5">
              <img src="/imgs/thebeat.png" alt="" id="thebeat-logo" />
            </div>
          </div>
          <div className="row justify-content-center align-items-center">
            <div className="col-6 col-sm-2 mb-5">
              <img src="/imgs/disrupt.png" alt="" id="disrupt-logo" />
            </div>
            <div className="col-6 col-sm-2 mb-5">
              <img src="/imgs/channels.png" alt="" id="channels-logo" />
            </div>
            <div className="col-6 col-sm-2 mb-5">
              <img src="/imgs/connect.png" alt="" id="connect-logo" />
            </div>
            <div className="col-6 col-sm-2 mb-5">
              <img src="/imgs/thenerve.png" alt="" id="thenerve-logo" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default HireVehiclesSection;
