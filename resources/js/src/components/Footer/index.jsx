import React from "react";

const Footer = () => {
  return (
    <footer>
      <section className="pt-5">
        <div className="container">
          <div className="row">
            <div className="col-md-2 mb-4">
              <h6>ABOUT US</h6>
              <ul className="mt-3">
                <li>
                  <a href>Our Narrative</a>
                </li>
                <li>
                  <a href>Invest</a>
                </li>
              </ul>
            </div>
            <div className="col-md-2 mb-4">
              <h6>SERVICES</h6>
              <ul className="mt-3">
                <li>
                  <a href>Hire Full term drivers</a>
                </li>
                <li>
                  <a href>Hire Short term driver</a>
                </li>
                <li>
                  <a href>Hire drivers for a day</a>
                </li>
                <li>
                  <a href>Hire drivers for a week</a>
                </li>
                <li>
                  <a href>Hire drivers for a month</a>
                </li>
                <li>
                  <a href>Annual reachouts</a>
                </li>
                <li>
                  <a href>Recruit by Myself</a>
                </li>
                <li>
                  <a href>Hire Uber drivers</a>
                </li>
                <li>
                  <a href>Hire Taxify drivers</a>
                </li>
                <li>
                  <a href>Hire dispatch Riders</a>
                </li>
                <li>
                  <a href>Hire vehicles</a>
                </li>
                <li>
                  <a href>Human Resource Plans</a>
                </li>
                <li>
                  <a href>Organise private transport</a>
                </li>
              </ul>
            </div>
            <div className="col-md-3 mb-4">
              <h6>TERMS & CONDITIONS</h6>
              <ul className="mt-3">
                <li>
                  <a href>Privacy Policy</a>
                </li>
                <li>
                  <a href>FAQs</a>
                </li>
                <li>
                  <a href>Mobile App</a>
                </li>
              </ul>
            </div>
            <div className="col-md-3 mb-4">
              <h6>SUPPLEMENTARY SERVICES</h6>
              <ul className="mt-3">
                <li>
                  <a href>Fleet Management</a>
                </li>
                <li>
                  <a href>Fleet Valuation</a>
                </li>
                <li>
                  <a href>Asset Valuation</a>
                </li>
                <li>
                  <a href>Vehicles for sold uber purposes</a>
                </li>
                <li>
                  <a href>Comprehensive Insurance</a>
                </li>
                <li>
                  <a href>Hotels Private transport</a>
                </li>
                <li>
                  <a href>Asset Management</a>
                </li>
                <li>
                  <a href>Training as a Service</a>
                </li>
                <li>
                  <a href>Verification as a Service</a>
                </li>
              </ul>
            </div>
            <div className="col-md-2 mb-4">
              <h6>CITIES</h6>
              <div className="row">
                <div className="col-6">
                  <ul className="mt-3">
                    <li>
                      <a href>Lagos</a>
                    </li>
                    <li>
                      <a href>Abuja</a>
                    </li>
                    <li>
                      <a href>Port Harcourt</a>
                    </li>
                    <li>
                      <a href>Ibadan</a>
                    </li>
                    <li>
                      <a href>Kaduna</a>
                    </li>
                    <li>
                      <a href>Uyo</a>
                    </li>
                    <li>
                      <a href>Abeokuta</a>
                    </li>
                    <li>
                      <a href>Benin</a>
                    </li>
                    <li>
                      <a href>Warri</a>
                    </li>
                    <li>
                      <a href>Kano</a>
                    </li>
                    <li>
                      <a href>Jos</a>
                    </li>
                    <li>
                      <a href>Benue</a>
                    </li>
                    <li>
                      <a href>Enugu</a>
                    </li>
                  </ul>
                </div>
                <div className="col-6">
                  <ul className="mt-3">
                    <li>
                      <a href>Aba</a>
                    </li>
                    <li>
                      <a href>Onitsha</a>
                    </li>
                    <li>
                      <a href>Calabar</a>
                    </li>
                    <li>
                      <a href>Asaba</a>
                    </li>
                    <li>
                      <a href>Ilorin</a>
                    </li>
                    <li>
                      <a href>Minna</a>
                    </li>
                    <li>
                      <a href>Akwa</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="row justify-content-end">
              <div className="col-md-4 mt-3 mb-4">
                <div className="social-contact">
                  <div className="app-logo__container">
                  <img src="/imgs/app-logo.png" alt="App Logo"/>
                  </div>
                  <div className="icons">
                    <a href><i className="fab fa-facebook-f"></i></a>
                    <a href><i className="fab fa-twitter"></i></a>
                    <a href><i className="fab fa-youtube"></i></a>
                    <a href><i className="fab fa-instagram"></i></a>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </section>
      <section className="pt-4 pb-4">
        <div className="container">
          <div className="text-center">
            <p>
              &copy; DriverdNG Recruitment Services Ltd. All Rights Reserved
            </p>
            <p>
              22, Ayodele street, off Ikorodu road, Fadeyi Bus-stop, Lagos,
              Nigeria.
            </p>
            <p>
              29, Mambilla street, off Aso drive, Asokoro, Maitana, Abuja,
              Nigeria. All rights reserved
            </p>
          </div>
        </div>
      </section>
    </footer>
  );
};

export default Footer;
