import React from "react";

const DownloadSection = () => {
  return (
    <section className="download-section pt-5">
      <div className="container">
        <h4 className="text-center pb-3">BOOK | ONDEMAND | RELAX</h4>
        <div className="row">
          <div className="col-sm-6">
            <img src="/imgs/phones.png" alt="Phones Images" />
          </div>
          <div className="col-sm-6">
            <div className="download-section__text">
              <p className="">
                Need a Mobility Solution; A Professional driver, Want to hire a
                vehicle, get an organised more or lift, Vehicle Insurance or
                Fleet Management?
              </p>

              <p className="pt-5">
                We are the Right platform where you can access already verified
                Professional drivers, Hire vehicles, get organised private
                transport, get comprehensive vehicle insurance and still enjoy
                guaranteed monthly payouts ehen you add your vehicles to our
                platform. Our services can be accessed on-demand and in real
                time
              </p>

              <h5 className="mt-4">Download Now</h5>
              <div className="store-img__container mt-4 mb-5">
                <div className="app-store__img">
                  <img src="/imgs/ios.png" alt="IOS Store"/>
                </div>
                <div className="app-store__img">
                  <img src="/imgs/android.png" alt="Android Store"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default DownloadSection;
