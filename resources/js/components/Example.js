import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import App from "../src/App"

export default class Example extends Component {
    render() {
        return (
            <div>
                <App />
            </div>
        );
    }
}

if (document.getElementById('element')) {
    ReactDOM.render(<Example />, document.getElementById('element'));
}
